package com.study.demo.service;

import com.study.demo.exceptions.Checked;
import com.study.demo.exceptions.Unchecked;

public class OperationServiceImpl implements OperationService {

	private DataService dataService;

	@Override
	public Boolean validGreatThan() {
		System.out.println("entra");

		Integer valueOne = dataService.greatThanFirst();
		System.out.println("acsa");
		Integer valueTwo = dataService.greatThanSecond();
		System.out.println("va1: " + valueOne + "value2: " + valueTwo);
		return (valueOne > valueTwo) ? true : false;
		// return (valueOne!=null) & (valueTwo !=null) & (valueOne>valueTwo)?true:false;
	}

	@Override
	public Boolean validChain() {

		String eqal = dataService.equalsStrings();

		if (eqal.contains("world")) {
			return true;
		}
		return false;

	}

	@Override
	public String operateChain() {
		String aux = dataService.chain();
		if (aux != null && !aux.isEmpty()) {
			return "si hay dato";
		}
		return "sin dato";
	}

	public int sum(int numbers[]) {
		int su = 0;
		for (int i : numbers) {
			su += i;
		}
		return su;
	}

	public String throwsExceptionUncheked() {

		String chainEmpty = dataService.chain();

		if (chainEmpty != null && !chainEmpty.isEmpty()) {
			return "cadena";
		}
		throw new Unchecked("Se lanza unchecked");
	}

	public int checkedExceptionInteger() throws Checked {
		int data = dataService.getNumber();

		if (data == 0) {
			throw new Checked("Se lanza checked");
		}
		return data;
	}
}
