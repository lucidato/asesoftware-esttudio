package com.study.demo.service;

public interface OperationService {

	public Boolean validGreatThan();
	
	public Boolean validChain();
	
	public String operateChain();
	
	public String throwsExceptionUncheked();
}
