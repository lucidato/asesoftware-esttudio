package com.study.demo.service;

public class CalculatorSerImpl implements CalculatorService {

	private DataService dataService;
	 
	@Override
	public double calculateAverage() {
		int [] numbers = dataService.getListOfNumbers();
		double aver= 0;
		
		for (int i: numbers) {
			aver += i;
		}
		return (numbers.length > 0) ? aver / numbers.length :0 ;
	}
	
}
