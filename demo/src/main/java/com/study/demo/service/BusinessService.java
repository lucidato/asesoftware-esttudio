package com.study.demo.service;

import org.springframework.stereotype.Service;

@Service
public class BusinessService {

	private DataService listService;
	
	public BusinessService (DataService listService) {
		this.listService = listService;
	}
	
	public int findTheGreatestAllData() {
		int [] data= listService.getListOfNumbers();
		int greatest = 0;
		
		for (int value: data) {
			greatest = value>greatest?value:greatest;
		}
		return greatest;
	}
}
