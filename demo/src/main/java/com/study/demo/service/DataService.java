package com.study.demo.service;

public interface DataService {
	
	public int[] getListOfNumbers();
	
	public int greatThanFirst(Integer first);
	public int greatThanSecond(Integer second);
	public int greatThanFirst();
	public int greatThanSecond();
	
	public int getNumber();
	
	public String equalsStrings ();
	
	public String chain ();

}
