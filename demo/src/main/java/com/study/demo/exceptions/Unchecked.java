package com.study.demo.exceptions;

public class Unchecked extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -211293007194601745L;

	public Unchecked(String message) {
		super(message);
	}
}
