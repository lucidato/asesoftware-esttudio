package com.study.demo.exceptions;

public class Checked extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 620194339715569664L;

	public Checked(String message) {
		super(message);
	}

}
