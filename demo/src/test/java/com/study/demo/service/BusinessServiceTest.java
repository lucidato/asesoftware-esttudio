package com.study.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BusinessServiceTest {

	@Mock
	private DataService dataService;
	
	@InjectMocks
	private BusinessService businessService;

	@Test
	public void givenAnArray_whenCalleService_thenReturnGreathesValue() {
		when(dataService.getListOfNumbers()).thenReturn(new int[] { 24, 15, 3 });
		assertEquals(24,businessService.findTheGreatestAllData());
	}
	
	@Test
	public void givenAnArrayDiferentOrder_whenCalleService_thenReturnGreathesValue() {
		when(dataService.getListOfNumbers()).thenReturn(new int[] { 3, 15, 33 });
		assertEquals(33,businessService.findTheGreatestAllData());
	}
}
