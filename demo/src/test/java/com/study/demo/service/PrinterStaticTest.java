//package com.study.demo.service;
//
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertTrue;
//import static org.mockito.Mockito.atLeast;
//import static org.mockito.Mockito.when;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.powermock.api.mockito.PowerMockito;
//import org.powermock.core.classloader.annotations.PrepareForTest;
//import org.powermock.modules.junit4.PowerMockRunner;
//
//@RunWith(PowerMockRunner.class)
//@PrepareForTest(PinterStatic.class)
//public class PrinterStaticTest {
//
//	@Test
//	public void givenBooleanMethod_whenCallPrint_thenReturn() {
//		
//		PowerMockito.mockStatic(PinterStatic.class);
//		when(PinterStatic.print("Printing")).thenReturn(true);
//		when(PinterStatic.print("")).thenReturn(Boolean.FALSE);
//		
//		assertTrue(PinterStatic.print("Printing"));
//		assertFalse(PinterStatic.print(""));
//		
//		PowerMockito.verifyStatic(PinterStatic.class, atLeast(2));
//		PinterStatic.print(toString());
//		
//	}
//}
