package com.study.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorServiceTest {

	protected static final double result_wish = 3.0; 
	protected static final double result_zero = 0;
	protected static final int [] array_values = {1,2,3,4,5};
	protected static final int [] array_empty = {};
	
	@InjectMocks
	private CalculatorSerImpl calculateroService;
	
	@Mock
	private DataService dataService;
	
	/**
	 * <Scenary> exist an array with different values
	 * <given> an array with 5 values
	 * <when> executes the service for calling method calculateAverage
	 * <then> the result is success with average
	 */
	@Test
	public void givenAnArrayWithValues_whenItsCallsService_thenReturnAverage () {
		when(dataService.getListOfNumbers()).thenReturn(array_values);
		assertEquals(result_wish, calculateroService.calculateAverage());
	}
	
	/**
	 * 
	 */
	@Test
	public void givenAnArayEmpty_whenistCallServivce_thenReturnAValue() {
		when(dataService.getListOfNumbers()).thenReturn(array_empty);
		assertEquals(result_zero,calculateroService.calculateAverage());
	}
 
}
