package com.study.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.study.demo.exceptions.Checked;
import com.study.demo.exceptions.Unchecked;

@RunWith(MockitoJUnitRunner.class)
public class OperationServiceTest {

	@Mock // simulate data de esa capa
	private DataService dataService;

	@InjectMocks // simlate response, inyecta las dpendencias respectivas
	private OperationServiceImpl operationService;

	@Test
	public void givenanarrayofInteger_whenexecuta_thenSumisSuccesfull() {
		assertEquals(6, operationService.sum(new int[] { 1, 2, 3 }));
	}

	@Test
	public void givenanarrayofInteger_whenexecuta_thenSumisIszero() {
		assertEquals(0, operationService.sum(new int[] {}));
	}

	@Test
	public void givenAChain_whenExecuteDataService_thenMessaggeThereData() {
		when(dataService.chain()).thenReturn("Informacion");
		assertEquals("si hay dato", operationService.operateChain());
	}

	/*
	 * Esta no sirve sin embargo pasa , aparece con una x en la consola, no genera
	 * error
	 * 
	 * @Test public void givenaChainEmpty_whenExecuteService_thenReturnMessage() {
	 * when(dataService.chain()).thenReturn(""); assertEquals("",
	 * operationService.operateChain()); }
	 */

	@Test
	public void givenaChainEmpty_whenExecuteService_thenReturnMessage() {
		when(dataService.chain()).thenReturn("");
		assertEquals("sin dato", operationService.operateChain());
	}

	@Test
	public void givenaChainNull_whenExecuteService_thenReturnMessage() {
		when(dataService.chain()).thenReturn(null);
		assertEquals("sin dato", operationService.operateChain());
	}

	@Test
	public void givenAchain_whenExecuteService_thenReturnTrue() {
		when(dataService.equalsStrings()).thenReturn("world");
		assertEquals(Boolean.TRUE, operationService.validChain());
	}

	@Test
	public void givenAChain_whenExecuteServiceWithoutWord_thenReturnFalse() {
		when(dataService.equalsStrings()).thenReturn("");
		assertEquals(false, operationService.validChain());
	}

	@Test
	public void givenAchainContains_whenExecuteService_thenReturnTrue() {
		when(dataService.equalsStrings()).thenReturn("sworlds");
		assertEquals(Boolean.TRUE, operationService.validChain());
	}

	@Test // esta no pasa porque nunca se valido en codigo el null
	public void givenNullChain_whenExecuteService_thenReturnFalse() {
		when(dataService.equalsStrings()).thenReturn(null);
		assertEquals(false, operationService.validChain());
	}

	/**
	 * <Scenary A string empty so throws unckecked exception>
	 * <given> a string empty</given>
	 * <when> call method with exception </when>
	 * <then>throws ecxption </then>
	 */
	@Test(expected = Unchecked.class)
	public void givenAStringEmpty_whenExeceuteThrowsExceptionUncheked_thenThrowsUnchekedException() {
		when(dataService.chain()).thenReturn("");
		assertEquals("", operationService.throwsExceptionUncheked());
		//assertNotEquals("Se lanza unchecked", operationService.throwsExceptionUncheked());
		//assertThatExceptionOfType(Unchecked.class);	
	}
	
	@Test// esta falla porque falta (expected = Unchecked.class) 
	public void givenAStringEmptyWithoutanotation_whenExeceuteThrowsExceptionUncheked_thenThrowsUnchekedException() {
		when(dataService.chain()).thenReturn("");
		assertEquals("", operationService.throwsExceptionUncheked());
	}
	
	@Test(expected = Unchecked.class)
	public void givenNullString_whenExecuteService_thenThrowsExpetion() {
		when(dataService.chain()).thenReturn(null);
		assertEquals(null, operationService.throwsExceptionUncheked());
	}
	
	@Test
	public void givenString_whenExceutethhrow_thenNormally() {
		when(dataService.chain()).thenReturn("dsada");
		assertEquals("cadena", operationService.throwsExceptionUncheked());
	}
	
	@Test (expected = Checked.class)
	public void givenAInteger_whenCallcheckedMethod_thenThrowsExceprtion() throws Checked {
		when(dataService.getNumber()).thenReturn(0);
		assertEquals(4, operationService.checkedExceptionInteger());
	}
	
	@Test (expected = Checked.class)
	public void givenAIntegerZero_whenCallcheckedMethod_thenThrowsExceprtion()  {
		when(dataService.getNumber()).thenReturn(0);
		try {
			//when(operationService.checkedExceptionInteger()).thenThrow(Checked.class);
			assertEquals(4, operationService.checkedExceptionInteger());
		} catch (Checked e) {
			 
			System.out.println(e.getMessage()+ ": ");	 
		}
	}
	
}
