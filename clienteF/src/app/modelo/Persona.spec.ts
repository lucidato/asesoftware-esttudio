import { Persona } from '../modelo/Persona';

describe('test for person', () => {

    beforeEach(() => {
        //
    });

    describe('Test for person get full name', () => {
        it ('should return an string with name + lastaname', () =>{
            const perso = new Persona("Fabian", "Pulido", 20);
            expect(perso.getFullName()).toEqual('Fabian Pulido');
        });
    });

    describe('Test for person geat age in years', () => {

        it('should return 34 years', () => {
            const persona = new Persona ('Fabian', 'Pulido', 24);
            const age = persona.getAgeInYears(10);
            expect(age).toEqual(34);
        });

        it ('should return 24 years', () => {
            const persona = new Persona ('', '', 4);
            const ag = persona.getAgeInYears(20);
            expect(ag).toEqual(24);
        });

        it ('should retunrr 20 ywars with negative number', () => {
            const persona = new Persona ('', '', 28);
            const age = persona.getAgeInYears(-10);
            expect(age).toEqual(28);
        });
    });
});
