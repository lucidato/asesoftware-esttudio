
export class Persona {

    constructor(
     public name: string,
     public lastname: string,
     public age: number
    ) {    }

    public getFullName(): string{
     return `${this.name} ${this.lastname}`;
    }

    public getAgeInYears( years: number ): number {
     return this.age + years;
    }
  }
